create database Shop

create table Item(
id_item int IDENTITY(1,1) not null primary key,
name_item nvarchar(20) not null,
isweight bit not null,
id_post int);

create table Postachalnik(
id_post int IDENTITY(1,1) not null primary key,
name nvarchar(20) not null,
address nvarchar(20) not null
);

insert into Item(name_item,isweight,id_post)
values('Lays',1,1),
('Pringles',1,2)

insert into Postachalnik(name,address)
values('LaysCompany','USA'),
('PringlesCompany','USA')

select * from Postachalnik
select * from Item

CREATE VIEW view2
AS
SELECT
    i.name_item,
    p.name,
    p.address
FROM
    dbo.Item AS i
JOIN
    dbo.Postachalnik AS p ON i.id_post = p.id_post;
